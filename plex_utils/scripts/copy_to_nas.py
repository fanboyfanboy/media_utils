# Simple script to copy a file to another server (e.g. /{host}/tmp_dl -> /{remote_host}/tmp/)
# @author fanboyfanboy
# Note: 
# 	Requires SSH key to be copied to remote server.


### Import Statements ###
from paramiko import SSHClient
from scp import SCPClient
import sys
import logging

### Setup ###

# Print values for logging purposes
logging.basicConfig(filename='copy.log',level=logging.DEBUG);
logging.info("Initialized logging module.")

# Intialize vars
script, destination, file_name = argv;
hdd_list = ['d1', 'd2', 'd3'];

# Determine remote path
if destination == drive_list(0):
	remote_path = '/mnt/bindmounts/shared/drive1/tmp/';
elif destination == drive_list(1):
	remote_path = '/mnt/bindmounts/shared/tmp/';
elif destionation == drive_list(2):
	remote_path == '/mnt/bindmounts/shared/drive3/tmp/';
else:
	print("Error occurred while attempting to determine remote path.");
	sys.exit();

logging.info("Variables intialized successfully.");
logging.debug("Script: "+script):
logging.debug("HDD Destination: "+destination);
logging.debug("File to be transfered: "+file_name);
logging.debug("Remote Path: "+remote_path);

# Create SSH Client 
ssh = SSHClient();
ssh.load_system_host_keys();
ssh.connect('root@'+destination);

# SCPClient takes a paramiko transport as n argument
scp = SCPClient(ssh.get_transport());

# Transfer file
scp.put(file_name,recursive=True,remote_path=remote_path);

# Don't forget to close
scp.close();